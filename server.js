const express = require("express");
const mysql = require("mysql");
const bodyParser = require("body-parser");
const bcrypt = require("bcrypt-nodejs");
const session = require("express-session");

const app = express();


app.use(bodyParser.json());
app.use(session({ secret: 'Tout est crypté' }));
var urlencodeparser = bodyParser.urlencoded({ extended: true });
app.use(urlencodeparser);

require("dotenv").config();


//-------connection de la base de données

const db = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: 'DevTonCV'
});
db.connect(function (err) {
  if (err) throw err;
  console.log("MySQL est connecté");
});
//------Fin de connexion à la bdd

app.set('view engine', 'ejs');
app.use(express.static("public"));

//------Début des routes

app.get('/', function (req, res) {
  res.render('accueil');
});
app.get('/connecter', function (req, res) {
  res.render('connexion');
});
app.get('/menuprincipal', function (req, res) {
  res.render('menuprincipal', {
    lastname: sess.lastname,
    firstname: sess.firstname,
    birthday: sess.birthday,
    email: sess.email,
    address: sess.address,
    postalCode: sess.postalCode,
    city: sess.city,
    cv: sess.cv,
    idUsers: sess.idUsers
  });
});

//------fin des routes

//-----Connexion

var sess;
app.post("/formconnect", urlencodeparser, function (req, res) {
  var sql = "SELECT `idUsers`, `lastname`, `firstname`, `birthday`, `email`, `address`, `postalCode`, `city`, CV.entilted AS CV FROM Users INNER JOIN DevTonCV.CV ON DevTonCV.CV.Users_idUsers = DevTonCV.Users.idUsers WHERE DevTonCV.Users.email = (?);";
  var values = [req.body.email];
  sql = mysql.format(sql, values);
  sess = req.session;
  sess.email = req.body.email;
  console.log(sess.email);
  db.query(sql, function (error, results, fields) {
    if (!error) {
      var msg = "Email ou mot de passe invalide";
      if (results.length != 1) {
        console.log(results);
        res.send(msg);
      } else {
        if (req.body.password) {
          logged_user = {
            email: results[0].email,
            lastname: results[0].lastname,
            firstname: results[0].firstname,
            birthday: results[0].birthday,
            address: results[0].address,
            postalCode: results[0].postalCode,
            city: results[0].city,
            idUsers: results[0].idUsers,
            cv: results[0].CV
          };
          console.log(logged_user);
          sess.lastname = logged_user.lastname;
          sess.firstname = logged_user.firstname;
          sess.birthday = logged_user.birthday;
          sess.address = logged_user.address;
          sess.postalCode = logged_user.postalCode;
          sess.city = logged_user.city;
          sess.cv = logged_user.cv;
          sess.idUsers = logged_user.idUsers;
          msg = "Bienvenue sur notre application";
          console.log("utilisateur connecté");
          res.redirect("/menuprincipal");
        } else {
          console.log(msg);
          res.redirect("/accueil");
        }
      }
    } else {
      res.send(error);
    }
  })
})
app.get("/hobbies", urlencodeparser, function (req, res) {
  var sql = "SELECT name, description FROM Hobbies INNER JOIN CV_has_Hobbies ON CV_has_Hobbies.Hobbies_idHobbies = Hobbies.idHobbies WHERE CV_has_Hobbies.CV_Users_idUsers = (?);";
  var values = sess.idUsers;
  sql = mysql.format(sql, values);
  sess = req.session;
  db.query(sql, function (error, results, fields) {
    console.log(results);
    let User_Hobbies = [];
    if (!error) {
      for (let i = 0; i < results.length; i++) {
        const hobbiesName = results[i].name;
        const hobbiesDescription = results[i].description;
        User_Hobbies[i] = {
          "name": hobbiesName,
          "description": hobbiesDescription
        }
      }
      res.render('hobbies', {
        Hobbies: User_Hobbies
      });
    } else {
      res.send(error);
    }
  })
})
app.get('/projets', urlencodeparser, function (req, res) {
  var sql = "SELECT idProjects, title, subtitle, link, description FROM Projects INNER JOIN CV_has_Projects ON CV_has_Projects.Projects_idProjects = Projects.idProjects WHERE CV_has_Projects.CV_Users_idUsers = (?);";
  var values = sess.idUsers;
  sql = mysql.format(sql,values);
  sess.req.session;
  db.query(sql, function (error, results, fields) {
    console.log(results);
    let Users_Projects = [];
    if (!error) {
      for (let i = 0; i < results.length ; i++){
        const ProjectsTitle = results[i].title;
        const ProjectsSubtitle = results[i].subtitle;
        const ProjectsLink = results[i].link;
        const ProjectsDescription = results[i].description;
        const ProjectsID = results[i].idProjects;
        Users_Projects[i] = {
          idProjects: ProjectsID,
          title : ProjectsTitle,
          subtitle : ProjectsSubtitle,
          link : ProjectsLink,
          description : ProjectsDescription
        }
      }
      res.render('projets', {
        Projects : Users_Projects
      })
    } else {
      res.send(error);
    }
  })
})
app.get("/projets/update/:idProjects", function (req, res) {
  console.log(req.params.idProjects);
  db.query('select * from Projects where idProjects=' + req.params.idProjects + ';', function (error, results, fields) {
      if (!error) {
          res.render('projets_update', { projets: results[0], idProjects:req.params.idProjects });
      }else{
        console.log("error");
        res.render(error);
      }
  })
})
app.post("/projets/edit", urlencodeparser, function(req, res) {
  var sql = "UPDATE Projects SET title = ?, subtitle = ?, link = ?, description = ? where idProjects=?;";
  var id = req.body.id;
  var values = [req.body.title, req.body.subtitle, req.body.link, req.body.description, id];
  sql = mysql.format(sql, values);
  db.query(sql, function(error, results, fields){
    if(!error){
      res.redirect("/projets/");
    } else {
      console.log(error);
      res.send(error);
    }
  })
})
//-----Fin de connexion


//-----Connection du serveur avec le port
app.listen(3080, function () {
  console.log('Le serveur ecoute sur le port 3080');
});



